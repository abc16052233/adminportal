﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.Planthire.API.Models
{
    public class TransactionWrapper
    {
        public TransactionWrapper()
        {
            ResultSet = new List<Object>();
            Messages = new List<string>();
        }

        public bool IsSuccess { get; set; }
        public List<string> Messages { get; set; }
        public List<Object> ResultSet { get; set; }
        public Object Result { get; set; }
        public string ImageResult { get; set; }
    }
}