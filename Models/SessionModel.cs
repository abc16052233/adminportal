﻿namespace Peercore.CRM.AdminPortal.API.Models
{
    public class CustPortalSession
	{
		public int SessionId { get; set; }
		public string ClientId { get; set; } = "";
		public DateTime SessionOnTiime { get; set; }
		public DateTime SessionOffTime { get; set; }
		public string LocationLat { get; set; } = "";
		public string LocationLng { get; set; } = "";
		public string Device { get; set; } = "";

		public List<CustPortalEvents> EventList { get; set; }
	}

	public class CustPortalEvents
	{
		public decimal EventId { get; set; }
		public int SessionId { get; set; }
		public string ActionType { get; set; } = "";
		public string Module { get; set; } = "";
		public string Description { get; set; } = "";
		public DateTime EventDate { get; set; }
	}

    //added by shalinka
    public class SessionModel
    {
        public int SessionId { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string SessionOnTime { get; set; }
        public string SessionOffTime { get; set; }
        public string LocationLat { get; set; }
        public string LocationLon { get; set; }
        public string Device { get; set; }
    }

    public class EventModel
    {
        public int EventId { get; set; }
        public int SessionId { get; set; }
        public string ActionType { get; set; }
        public string Module { get; set; }
        public string Description { get; set; }
        public string EventDate { get; set; }
    }

    public class UserEventsCount
    {
        public string UserId { get; set; }
        public string Module { get; set; }
        public int EventCount { get; set; }
    }
}
