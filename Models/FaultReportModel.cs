﻿namespace Peercore.CRM.AdminPortal.API.Models
{
    public class FaultReportModel
    {
        public int FrId { get; set; }
        public int PlantNo { get; set; }
        public string PlantCode { get; set; }
        public string PlantDescription { get; set; }
        public int OrderId { get; set; }
        public string MeterReading { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public int SubmittedById { get; set; }
        public string SubmittedBy { get; set; }
        public string ContactNo { get; set; }
        public string SubmittedDate { get; set; }
        public string SubmittedType { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string Severity { get; set; }
        public string Location { get; set; }
        public string WorkOrderNo { get; set; }
        //public string ResolvedBy { get; set; }
        public string ResolvedDate { get; set; }

        public FaultReportModel() 
        {

        }
    }
}
