﻿namespace Peercore.CRM.AdminPortal.API.Models
{
    public class UserModel
    {
        public string User_Id { get; set; }

        //public int PImageNumber { get; set; }
        //public int BImageNumber { get; set; }
        public byte[] PImage { get; set; }
        public byte[] BImage { get; set; }
        public Guid PImageName { get; set; }
        public Guid BImageName { get; set; }
    }
}
