﻿namespace Peercore.CRM.AdminPortal.API.Models
{
    public class OperatorProfileModel
    {
        public int EmployeeNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string ContactNumber { get; set; }
        public string ContactEmail { get; set; }
        public string ProfileImage { get; set; }
        public string GDriveLink { get; set; }
        public string Skills { get; set; }
        public List<OperatorSkillModel> OperatorsSkills { get; set; }
    }

    public class OperatorSkillModel
    {
        public string SkillsCode { get; set; }
        public string SkillsDescription { get; set; }
    }
}
