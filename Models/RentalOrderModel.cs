﻿namespace Peercore.CRM.AdminPortal.API.Models
{
    public class RentalOrderModel
    {
        public int OrderId { get; set; } = 0;
        public string ProjectName { get; set; }
        public string CustomerCode { get; set; }
        public string AllocateFrom { get; set; }
        public string AllocateTo { get; set; }
        public string CatalogCode { get; set; }
        public string CatalogType { get; set; }
        public string EquipmentCode { get; set; }
        public string EquipmentDescription { get; set; }

        public RentalOrderModel() 
        { 

        }

    }
}
