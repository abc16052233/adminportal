﻿namespace Peercore.CRM.AdminPortal.API.Models
{
    public class CheckListHeaderModel
    {
        public int PclId { get; set; }
        public string PlantCode { get; set; }
        public string PlantNo { get; set; }
        public string MeterReading { get; set; }
        public string ROID { get; set; }
        public string CustomerName { get; set; }
        public string Project { get; set; }
        public string OperatorFullName { get; set; }
        public string CompletedType { get; set; }
        public string CompletedBy { get; set; }
        public string ApprovedBy { get; set; }
        //public string CompletedId { get; set; }
        public string ContactNo { get; set; }
        public string Response { get; set; }
        public string WorkOrderNo { get; set; }
        public string WorkOrderStatus { get; set; }
        public string CompletedDate { get; set; }
        public string ServiceGroup { get; set; }
        public List<CheckListDetailModel> Checklist { get; set; }
    }

    public class CheckListResponsesModel
    {
        public int PclId { get; set; }
        public string Response { get; set; }
        public int ResponseCount { get; set; }
        
    }

    public class CheckListDetailModel
    {
        public int PclId { get; set; }
        public string PclCode { get; set; }
        public int ItemOrder { get; set; }
        public string Description { get; set; }
        public string Comment { get; set; }
        public string Response { get; set; }

    }

    public class CheckListOverviewRequestModel
    {
        public string CustomerCode { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
    }

    public class CheckListOverviewResponseModel
    {
        public string? Date { get; set; }
        public List<PlantDetailsOverview> Plants { get; set; } = new List<PlantDetailsOverview>();
    }

    public class PlantDetailsOverview
    {
        public int plantNo { get; set; }
        public string? plantCode { get; set; }
        public int OrderId { get;set; }
        public string? OrderType { get; set; }
        public int ChecklistID { get; set; }
    }
}
