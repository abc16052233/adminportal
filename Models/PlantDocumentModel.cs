﻿namespace Peercore.CRM.AdminPortal.API.Models
{
    public class PlantDocumentModel
    {
        public int EquipmentNo { get; set; }
        public string EquipmentCode { get; set; }
        public string GoogleDriveLink { get; set; }

        public PlantDocumentModel()
        {

        }
    }

    public class MachineInfoModel
    {
        //For plant 
        public int EquipmentNo { get; set; }
        public string EquipmentCode { get; set; }
        public string PlantDescription { get; set; }
        public string Comment { get; set; }
        public string ManufacturerDetails { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public string SerialNo { get; set; }
        public string EngineNo { get; set; }
        public string RegNo { get; set; }
        public string RegExpDate { get; set; }
        public string VersaHoldingPPSR { get; set; }

        //For detail
        public string RiskAssessmentExpiryDate { get; set; }
        public string Insurer { get; set; }
        public string InsurancePolicy { get; set; }
        public string ExpiryDate { get; set; }
        public int InsuranceValue { get; set; }

        //For gdrive documents
        public List<PlantDocumentInfoModel> PlantDocument { get; set; }

        //For Service History
        public List<ServiceHistoryModel> ServiceHistory { get; set; }
    }

    public class PlantDocumentInfoModel
    {
        //For gdrive
        public string Title { get; set; }
        public string GDDescription { get; set; }
        public string GoogleDriveLink { get; set; }

    }

    public class ServiceHistoryModel
    {
        //For service history
        public int ServiceID { get; set; }
        public int EquipmentNo { get; set; }
        public string EquipmentCode { get; set; }
        public string ServiceType { get; set; }
        public string MachineHours { get; set; }
        public string StartTime { get; set; }
        public string LocationLatitude { get; set; }
        public string LocationLongitude { get; set; }
        public string Suburb { get; set; }
        public string Comments { get; set; }
        public string UserName { get; set; }
        public string ServiceImage { get; set; }

    }


}
