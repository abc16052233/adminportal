﻿namespace Peercore.CRM.Planthire.API.Models
{
    public class LoginModel
    {
		public int SessionId { get; set; }
        public string CustCode { get; set; }
		public string CustName { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string PostCode { get; set; }
		public string Country { get; set; }
		public string Acn { get; set; }
		public string Abn { get; set; }
		public string Telephone { get; set; }
		public string Fax { get; set; }
		public string Mobile { get; set; }
		public string IternetAdd { get; set; }
		public string Contact { get; set; }
	}
}