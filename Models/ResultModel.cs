﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.Planthire.API.Models
{
    public class ResultModel
    {
        /// <summary>
        /// rowCount(fetch row Count)
        /// </summary>
        public int RowCount { get; set; }
        /// <summary>
        /// result(fetch result)
        /// </summary>
        public int Result { get; set; }
    }
}