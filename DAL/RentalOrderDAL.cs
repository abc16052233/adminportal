﻿using Peercore.CRM.AdminPortal.API.CommonUtility;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System.Data;
using System.Data.SqlClient;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public class RentalOrderDAL:IRentalOrderDAL
    {
        private readonly string _connectionStr;

        public RentalOrderDAL(IConfiguration configuration)
        {
            _connectionStr = configuration.GetConnectionString("HTLite_DB_ConnectionStr");
        }

        public async Task<TransactionWrapper> GetRentalOrderInforToCustomerPortal(string toDate, string fromDate, string custCode, string catalogType = "")
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<RentalOrderModel> rentalOrderDetailList = new List<RentalOrderModel>();


            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetRentalOrderInforToCustomerPortal", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ToDate", toDate);
                        cmd.Parameters.AddWithValue("@FromDate", fromDate);
                        cmd.Parameters.AddWithValue("@CustomerCode", custCode);
                        cmd.Parameters.AddWithValue("@CatlogType", catalogType);

                        await HelperUtility.GetRecords(ReadRentalOrderDetailDataRow, rentalOrderDetailList, cmd);

                        if (rentalOrderDetailList.Any())
                        {
                            IEnumerable<RentalOrderModel> _rentalOrderDetailList = ReadRentalOrderDetailDataGrouping(rentalOrderDetailList);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _rentalOrderDetailList;


                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;

        }

        private RentalOrderModel ReadRentalOrderDetailDataRow(IDataRecord record)
        {
            return new RentalOrderModel()
            {
                OrderId = HelperUtility.ReadNumberValue(record, "OrderID"),
                ProjectName = HelperUtility.ReadStringValue(record, "Project").Trim(),
                CustomerCode = HelperUtility.ReadStringValue(record, "Customer_Code").Trim(),
                AllocateFrom = HelperUtility.ReadDateValue(record, "AllocateFrom").ToString("dd/MM/yyyy"),
                AllocateTo = HelperUtility.ReadDateValue(record, "AllocateTo").ToString("dd/MM/yyyy"),
                CatalogCode = HelperUtility.ReadStringValue(record, "Catlog_Code").Trim(),
                CatalogType = HelperUtility.ReadStringValue(record, "CatlogType").Trim(),
                EquipmentCode = HelperUtility.ReadStringValue(record, "Equip_Code").Trim(),
                EquipmentDescription = HelperUtility.ReadStringValue(record, "Equip_Description").Trim(),

            };
        }

        private static IEnumerable<RentalOrderModel> ReadRentalOrderDetailDataGrouping(IEnumerable<RentalOrderModel> rentalOrderData)
        {
            IEnumerable<RentalOrderModel> data = new List<RentalOrderModel>();
            data = rentalOrderData;

            return data;
        }
    }
}
