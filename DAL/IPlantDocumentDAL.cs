﻿using Peercore.CRM.Planthire.API.Models;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public interface IPlantDocumentDAL
    {
        Task<TransactionWrapper> GetPlantDocumentUrl(int plantNo);
        Task<TransactionWrapper> GetMachineInfo(int plantNo);
    }
}
