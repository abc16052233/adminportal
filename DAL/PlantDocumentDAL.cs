﻿using Peercore.CRM.AdminPortal.API.CommonUtility;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Linq;
using System.Net;
using System;
using System.IO;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public class PlantDocumentDAL : IPlantDocumentDAL
    {
        private readonly string _connectionStr;
        private readonly string baseImageUrl;

        public PlantDocumentDAL(IConfiguration configuration)
        {
            _connectionStr = configuration.GetConnectionString("HTLite_DB_ConnectionStr");
            baseImageUrl = configuration.GetSection("AppSettings")["ServiceBaseImageUrl"];
        }

        //Get plant doc url
        public async Task<TransactionWrapper> GetPlantDocumentUrl(int plantNo)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<PlantDocumentModel> plantDocumentList = new List<PlantDocumentModel>();


            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetPlantDocUrl", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@EquipNo", plantNo );

                        await HelperUtility.GetRecords(ReadPlantDocumentListDataRow, plantDocumentList, cmd);

                        if (plantDocumentList.Any())
                        {
                            IEnumerable<PlantDocumentModel> _plantDocumentList = ReadPlantDocumentListDataGrouping(plantDocumentList);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _plantDocumentList;


                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;


        }

        private PlantDocumentModel ReadPlantDocumentListDataRow(IDataRecord record)
        {
            return new PlantDocumentModel()
            {
                EquipmentNo = HelperUtility.ReadNumberValue(record, "equip_no"),
                EquipmentCode = HelperUtility.ReadStringValue(record, "equip_code").Trim(),
                GoogleDriveLink = HelperUtility.ReadStringValue(record, "GD_Link").Trim(),

            };
        }

        private static IEnumerable<PlantDocumentModel> ReadPlantDocumentListDataGrouping(IEnumerable<PlantDocumentModel> plantDocumentList)
        {
            IEnumerable<PlantDocumentModel> data = new List<PlantDocumentModel>();
            data = plantDocumentList;

            return data;
        }


        //Get Machine Information
        public async Task<TransactionWrapper> GetMachineInfo(int plantNo)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<MachineInfoModel> machineInfoList = new List<MachineInfoModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetMachineInfo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@EquipNo", plantNo);

                        await HelperUtility.GetRecords(ReadMachineInfoDataRow, machineInfoList, cmd);

                        if (machineInfoList.Any())
                        {
                            IEnumerable<MachineInfoModel> _machineInfoList = ReadMachineInfoDataGrouping(machineInfoList);

                            List<PlantDocumentInfoModel> updatedPlantDocumentList = new List<PlantDocumentInfoModel>();
                            List<ServiceHistoryModel> updatedMachineInfoList = new List<ServiceHistoryModel>();

                            foreach (var item in _machineInfoList)
                            {
                                var updatedItem1 = await GetDocumentForMachineInfo(item.EquipmentNo);
                                updatedPlantDocumentList = (List<PlantDocumentInfoModel>)updatedItem1.Result;
                                item.PlantDocument = updatedPlantDocumentList;

                                var updatedItem2 = await GetServiceHistory(item.EquipmentNo);
                                updatedMachineInfoList = (List<ServiceHistoryModel>)updatedItem2.Result;
                                item.ServiceHistory = updatedMachineInfoList;
                            }

                            wrapper.IsSuccess = true;
                            wrapper.Result = _machineInfoList;


                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;


        }

        private MachineInfoModel ReadMachineInfoDataRow(IDataRecord record)
        {
            return new MachineInfoModel()
            {
                EquipmentNo = HelperUtility.ReadNumberValue(record, "equip_no"),
                EquipmentCode = HelperUtility.ReadStringValue(record, "equip_code").Trim(),
                PlantDescription = HelperUtility.ReadStringValue(record, "description").Trim(),
                Comment = HelperUtility.ReadStringValue(record, "comments").Trim(),
                ManufacturerDetails = HelperUtility.ReadStringValue(record, "manuf_details").Trim(),
                Make = HelperUtility.ReadStringValue(record, "make").Trim(),
                Model = HelperUtility.ReadStringValue(record, "model_no").Trim(),
                Year = HelperUtility.ReadNumberValue(record, "year"),
                SerialNo = HelperUtility.ReadStringValue(record, "serial_no"),
                EngineNo = HelperUtility.ReadStringValue(record, "engine_no").Trim(),
                RegNo = HelperUtility.ReadStringValue(record, "reg_no").Trim(),
                RegExpDate = HelperUtility.ReadDateValue(record, "reg_date").ToString("dd/MM/yyyy"),
                VersaHoldingPPSR = HelperUtility.ReadStringValue(record, "ppsr_value"),

                RiskAssessmentExpiryDate = HelperUtility.ReadDateValue(record, "risk_assessment_date").ToString("dd/MM/yyyy"),
                Insurer = HelperUtility.ReadStringValue(record, "insurer").Trim(),
                InsurancePolicy = HelperUtility.ReadStringValue(record, "insurance_policy_no").Trim(),
                ExpiryDate = HelperUtility.ReadDateValue(record, "insurance_exp_date").ToString("dd/MM/yyyy"),
                InsuranceValue = HelperUtility.ReadNumberValue(record, "Insurance_current_value"),
            };
        }

        private static IEnumerable<MachineInfoModel> ReadMachineInfoDataGrouping(IEnumerable<MachineInfoModel> machineInfoList)
        {
            IEnumerable<MachineInfoModel> data = new List<MachineInfoModel>();
            data = machineInfoList;

            return data;
        }

        //Get plant document for machine info
        public async Task<TransactionWrapper> GetDocumentForMachineInfo(int plantNo)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<PlantDocumentInfoModel> plantDocumentList = new List<PlantDocumentInfoModel>();


            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetDocumentForMachineInfo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PlantNo", plantNo);

                        await HelperUtility.GetRecords(ReadDocumentForMachineInfoDataRow, plantDocumentList, cmd);

                        if (plantDocumentList.Any())
                        {
                            IEnumerable<PlantDocumentInfoModel> _plantDocumentList = ReadDocumentForMachineInfoDataGrouping(plantDocumentList);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _plantDocumentList;


                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;


        }

        private PlantDocumentInfoModel ReadDocumentForMachineInfoDataRow(IDataRecord record)
        {
            return new PlantDocumentInfoModel()
            {
                Title = HelperUtility.ReadStringValue(record, "Title").Trim(),
                GDDescription = HelperUtility.ReadStringValue(record, "Description").Trim(),
                GoogleDriveLink = HelperUtility.ReadStringValue(record, "GD_Link").Trim(),

            };
        }

        private static IEnumerable<PlantDocumentInfoModel> ReadDocumentForMachineInfoDataGrouping(IEnumerable<PlantDocumentInfoModel> plantDocumentList)
        {
            IEnumerable<PlantDocumentInfoModel> data = new List<PlantDocumentInfoModel>();
            data = plantDocumentList;

            return data;
        }

        //Get Service History
        public async Task<TransactionWrapper> GetServiceHistory(int plantNo)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<ServiceHistoryModel> serviceHistoryDetailList = new List<ServiceHistoryModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetServiceInfoByPlantNo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@EquipNo", plantNo);

                        await HelperUtility.GetRecords(ReadServiceHistoryDataRow, serviceHistoryDetailList, cmd);

                        if (serviceHistoryDetailList.Any())
                        {
                            IEnumerable<ServiceHistoryModel> _serviceHistoryList = ServiceHistoryDataGrouping(serviceHistoryDetailList);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _serviceHistoryList;
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetServiceHistory: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        //Get service image url for service history
        private string ConvertImageUrlToBase64(string serviceBaseImageUrl2, string serviceImageName, string fileExtension)
        {
            string serviceImageUrl = ConstructImageUrl(serviceBaseImageUrl2, serviceImageName, fileExtension);

            if (serviceImageUrl != "")
            {
                try
                {

                    using (WebClient webClient = new WebClient())
                    {
                        byte[] imageBytes = webClient.DownloadData(serviceImageUrl);
                        return Convert.ToBase64String(imageBytes);
                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Error converting image URL to base64: {ex.Message}");
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        private string ConstructImageUrl(string serviceBaseImageUrl1, string serviceImageName, string fileExtension)
        {
            return $"{serviceBaseImageUrl1}/{serviceImageName}.{fileExtension}";
        }

        private  ServiceHistoryModel ReadServiceHistoryDataRow(IDataRecord record) 
        {
            string ServiceImageName = HelperUtility.ReadStringValue(record, "ImageName").Trim();
            string fileExtension = "jpeg";

            return new ServiceHistoryModel()
            {
                ServiceID = HelperUtility.ReadNumberValue(record, "ServiceID"),
                EquipmentNo = HelperUtility.ReadNumberValue(record, "EquipNo"),
                EquipmentCode = HelperUtility.ReadStringValue(record, "EquipCode").Trim(),
                ServiceType = HelperUtility.ReadStringValue(record, "ServiceType").Trim(),
                MachineHours = HelperUtility.ReadStringValue(record, "MachineHours").Trim(),
                StartTime = HelperUtility.ReadDateValue(record, "StartTime").ToString("dd/MM/yyyy"),
                LocationLatitude = HelperUtility.ReadStringValue(record, "Location_Lat").Trim(),
                LocationLongitude = HelperUtility.ReadStringValue(record, "Location_Long").Trim(),
                Suburb = HelperUtility.ReadStringValue(record, "Suburb").Trim(),
                Comments = HelperUtility.ReadStringValue(record, "Comments").Trim(),
                UserName = HelperUtility.ReadStringValue(record, "Username").Trim(),
                ServiceImage = ConvertImageUrlToBase64(baseImageUrl, ServiceImageName, fileExtension),

            };
        }

        private static IEnumerable<ServiceHistoryModel> ServiceHistoryDataGrouping(IEnumerable<ServiceHistoryModel> serviceHistoryData)
        {
            IEnumerable<ServiceHistoryModel> data = new List<ServiceHistoryModel>();
            data = serviceHistoryData;

            return data;
        }

    }
}
