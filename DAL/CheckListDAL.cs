﻿using Microsoft.AspNetCore.Http;
using Peercore.CRM.AdminPortal.API.CommonUtility;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System.Data;
using System.Data.SqlClient;
using System.Numerics;
using System.Reflection;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public class CheckListDAL : ICheckListDAL
    {
        private readonly string _connectionStr;

        public CheckListDAL(IConfiguration configuration)
        {
            _connectionStr = configuration.GetConnectionString("HTLite_DB_ConnectionStr");
        }

        public async Task<TransactionWrapper> GetAllPCLHeaderByDateAndType(string custCode, string startDate = "", string endDate = "", string clientType = "", string responseType = "")
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<CheckListHeaderModel> checkListHeader = new List<CheckListHeaderModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetAllPCLHeaderByDateAndType", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CustCode", custCode);
                        cmd.Parameters.AddWithValue("@StartDate", startDate);
                        cmd.Parameters.AddWithValue("@EndDate", endDate);
                        cmd.Parameters.AddWithValue("@ClientType", clientType);
                        cmd.Parameters.AddWithValue("@ResponseType", responseType);


                        await HelperUtility.GetRecords(ReadPreStartCheckListHeaderDataRow, checkListHeader, cmd);

                        

                        if (checkListHeader.Any())
                        {
                            IEnumerable<CheckListHeaderModel> _checkList = PreStartCheckListHeaderDataGrouping(checkListHeader);

                            List<CheckListDetailModel> updatedCheckList = new List<CheckListDetailModel>();
                            foreach (var item in _checkList)
                            {
                                var updatedItem = await GetAllPCLDetailsByPCLId(item.PclId);
                                updatedCheckList = (List<CheckListDetailModel>)updatedItem.Result;
                                item.Checklist = updatedCheckList;
                            }


                            wrapper.IsSuccess = true;
                            wrapper.Result = _checkList;

                            
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        public async Task<TransactionWrapper> GetAllPCLHeaderByCustomerCode(string custCode, string startDate = "", string endDate = "", string clientType = "", string responseType = "")
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<CheckListHeaderModel> checkListHeader = new List<CheckListHeaderModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetAllPCLHeaderByCustomerCode", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CustCode", custCode);
                        cmd.Parameters.AddWithValue("@StartDate", startDate);
                        cmd.Parameters.AddWithValue("@EndDate", endDate);
                        cmd.Parameters.AddWithValue("@ClientType", clientType);
                        cmd.Parameters.AddWithValue("@ResponseType", responseType);


                        await HelperUtility.GetRecords(ReadPreStartCheckListHeaderDataRow, checkListHeader, cmd);

                        if (checkListHeader.Any())
                        {
                            wrapper.IsSuccess = true;
                            wrapper.Result = checkListHeader;
                        }


                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        public async Task<TransactionWrapper> GetAllPCLHeaderByPCLId(string custCode, string filterOption = "", string filterValue = "")
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<CheckListHeaderModel> checkListHeader = new List<CheckListHeaderModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetAllPCLHeaderByPCLId", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CustCode", custCode);
                        cmd.Parameters.AddWithValue("@FilterOption", filterOption);
                        cmd.Parameters.AddWithValue("@FilterValue", filterValue);

                        await HelperUtility.GetRecords(ReadPreStartCheckListHeaderDataRow, checkListHeader, cmd);

                        if (checkListHeader.Any())
                        {
                            IEnumerable<CheckListHeaderModel> _checkList = PreStartCheckListHeaderDataGrouping(checkListHeader);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _checkList;
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetAllPCLHeaderByPCLId: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        private static CheckListHeaderModel ReadPreStartCheckListHeaderDataRow(IDataRecord record)
        {
            return new CheckListHeaderModel()
            {
                PclId = HelperUtility.ReadNumberValue(record, "PCL_ID"),
                PlantCode = HelperUtility.ReadStringValue(record, "equip_code").Trim(),
                PlantNo = HelperUtility.ReadStringValue(record, "equip_no").Trim(),
                MeterReading = HelperUtility.ReadStringValue(record, "MeterReading").Trim(),
                ROID = HelperUtility.ReadStringValue(record, "RO_ID").Trim(),
                CustomerName = HelperUtility.ReadStringValue(record, "Customer_Name").Trim(),
                Project = HelperUtility.ReadStringValue(record, "Project").Trim(),
                OperatorFullName = HelperUtility.ReadStringValue(record, "OperatorFullName").Trim(),
                CompletedType = HelperUtility.ReadStringValue(record, "CompletedType").Trim(),
                CompletedBy = HelperUtility.ReadStringValue(record, "CompletedBy").Trim(),
                ApprovedBy = HelperUtility.ReadStringValue(record, "ApprovedBy").Trim(),
                //CompletedId = HelperUtility.ReadStringValue(record, "CompletedId").Trim(),
                ContactNo = HelperUtility.ReadStringValue(record, "contact_no").Trim(),
                Response = HelperUtility.ReadStringValue(record, "Response").Trim(),
                WorkOrderNo = HelperUtility.ReadStringValue(record, "WorkOrderNo").Trim(),
                WorkOrderStatus = HelperUtility.ReadStringValue(record,"WorkOrderStatus").Trim(),
                CompletedDate = HelperUtility.ReadDateValue(record, "CompletedDate").ToString("dd/MM/yyyy"),
                ServiceGroup = HelperUtility.ReadStringValue(record, "ServiceGroup").Trim()

            };
        }

        private static IEnumerable<CheckListHeaderModel> PreStartCheckListHeaderDataGrouping(IEnumerable<CheckListHeaderModel> clistData)
        {
            IEnumerable<CheckListHeaderModel> data = new List<CheckListHeaderModel>();
            data = clistData;

            return data;
        }

        //public async Task<TransactionWrapper> GetPCLResponsesByPCLId(string pclId = "")
        //{
        //    TransactionWrapper wrapper = new TransactionWrapper();
        //    List<CheckListResponsesModel> checkListResponses = new List<CheckListResponsesModel>();

        //    using (SqlConnection connection = new SqlConnection(_connectionStr))
        //    {
        //        try
        //        {
        //            connection.Open();

        //            using (SqlCommand cmd = new SqlCommand("GetPCLResponsesByPCLId", connection))
        //            {
        //                cmd.CommandType = CommandType.StoredProcedure;
        //                cmd.Parameters.AddWithValue("@PCLId", pclId);

        //                await HelperUtility.GetRecords(ReadPreStartCheckListResponsesDataRow, checkListResponses, cmd);

        //                if (checkListResponses.Any())
        //                {
        //                    IEnumerable<CheckListResponsesModel> _checkList = PreStartCheckListResponsesDataGrouping(checkListResponses);

        //                    wrapper.IsSuccess = true;
        //                    wrapper.Result = _checkList;
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            wrapper.IsSuccess = false;
        //            wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //    }

        //    return wrapper;
        //}

        //private static CheckListResponsesModel ReadPreStartCheckListResponsesDataRow(IDataRecord record)
        //{
        //    return new CheckListResponsesModel()
        //    {
        //        PclId = HelperUtility.ReadNumberValue(record, "PCL_ID"),
        //        Response = HelperUtility.ReadStringValue(record, "Response").Trim(),
        //        ResponseCount = HelperUtility.ReadNumberValue(record, "ResponseCount"),
        //    };
        //}

        //private static IEnumerable<CheckListResponsesModel> PreStartCheckListResponsesDataGrouping(IEnumerable<CheckListResponsesModel> clistData)
        //{
        //    IEnumerable<CheckListResponsesModel> data = new List<CheckListResponsesModel>();
        //    data = clistData;

        //    return data;
        //}

        public async Task<TransactionWrapper> GetAllPCLDetailsByPCLId(int PclId)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<CheckListDetailModel> checkListDetail = new List<CheckListDetailModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("ListPreChecklistResonses", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PCL_ID", PclId);

                        await HelperUtility.GetRecords(ReadPreStartCheckListDetailsDataRow, checkListDetail, cmd);

                        if (checkListDetail.Any())
                        {
                            IEnumerable<CheckListDetailModel> _checkList = PreStartCheckListDetailsDataGrouping(checkListDetail);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _checkList;
                        }
                    }
                }     
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetAllPCLDetailsByPCLId: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        private static CheckListDetailModel ReadPreStartCheckListDetailsDataRow(IDataRecord record)
        {
            return new CheckListDetailModel()
            {
                PclId = HelperUtility.ReadNumberValue(record, "PCL_ID"),
                PclCode = HelperUtility.ReadStringValue(record, "PCL_Code").Trim(),
                ItemOrder = HelperUtility.ReadNumberValue(record, "ItemOrder"),
                Description = HelperUtility.ReadStringValue(record, "Description").Trim(),
                Comment = HelperUtility.ReadStringValue(record, "Comment").Trim(),
                Response = HelperUtility.ReadStringValue(record, "Response").Trim()
            };
        }

        private static IEnumerable<CheckListDetailModel> PreStartCheckListDetailsDataGrouping(IEnumerable<CheckListDetailModel> clistData)
        {
            IEnumerable<CheckListDetailModel> data = new List<CheckListDetailModel>();
            data = clistData;

            return data;
        }

        public async Task<TransactionWrapper> GetChecklistOverview(string CustomerCode, List<string> datestringArray)
        {
            TransactionWrapper wrapper = new TransactionWrapper();

            // decoded customer code
            byte[] data = Convert.FromBase64String(CustomerCode);
            string decodedCustomerCode = System.Text.Encoding.UTF8.GetString(data);

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    Dictionary<string, List<PlantDetailsOverview>> resultData = new Dictionary<string, List<PlantDetailsOverview>>();

                    foreach (string date in datestringArray)
                    {
                        // the day Of Week for given date
                        DateTime providedDate = DateTime.Parse(date);
                        DayOfWeek dayOfWeekForDate = providedDate.DayOfWeek;
                        string day = dayOfWeekForDate.ToString();

                        using (SqlCommand cmd = new SqlCommand("ChecklistOverview", connection))
                        {
                            List<PlantDetailsOverview> plantDetails = new List<PlantDetailsOverview>();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@CustomerCode", decodedCustomerCode);
                            cmd.Parameters.AddWithValue("@Date", date);
                            cmd.Parameters.AddWithValue("@DayOfWeekForDate", day);

                            await HelperUtility.GetRecords(ReadChecklistOverviewDataRow, plantDetails, cmd);

                            // Group plants by date
                            resultData[date] = plantDetails;
                        }
                    }

                    if (resultData.Any())
                    {
                        wrapper.IsSuccess = true;
                        wrapper.ResultSet = resultData.Select(item => new CheckListOverviewResponseModel
                        {
                            Date = item.Key,
                            Plants = item.Value.Select(plant => new PlantDetailsOverview
                            {
                                plantNo = plant.plantNo,
                                plantCode = plant.plantCode,
                                OrderId = plant.OrderId,
                                OrderType = plant.OrderType,
                                ChecklistID = plant.ChecklistID
                            }).ToList()
                        }).Cast<object>().ToList();
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetChecklistOverview: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }



        private static PlantDetailsOverview ReadChecklistOverviewDataRow(IDataRecord record)
        {
            return new PlantDetailsOverview()
            {
                plantNo = HelperUtility.ReadNumberValue(record, "PlantNo"),
                plantCode = HelperUtility.ReadStringValue(record, "PlantCode").Trim(),
                OrderId = HelperUtility.ReadNumberValue(record, "OrderId"),
                OrderType = HelperUtility.ReadStringValue(record, "OrderType").Trim(),
                ChecklistID = HelperUtility.ReadNumberValue(record, "ChecklistID")
            
            };
        }
        

        //private static IEnumerable<CheckListOverviewResponseModel> ChecklistOverviewDataGrouping(IEnumerable<CheckListOverviewResponseModel> checklistoverviewData)
        //{
        //    IEnumerable<CheckListOverviewResponseModel> data = new List<CheckListOverviewResponseModel>();
        //    data = checklistoverviewData;

        //    return data;
        //}
    }
}