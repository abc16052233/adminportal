﻿using System.Data.SqlClient;
using System.Data;
using Peercore.CRM.AdminPortal.API.Models;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public class UserDAL : IUserDAL
    {
        //delegate T ReadRow<T>(IDataRecord record);

        private readonly string _connectionStr;
        private readonly IConfiguration _config;
        private readonly ILogger<UserDAL> _logger;

        public UserDAL(IConfiguration config, ILogger<UserDAL> logger)
        {
            _connectionStr = config.GetConnectionString("HTLite_DB_ConnectionStr");
            _logger = logger;
            _config = config;
        }

        public async Task<bool> SaveUserImage(UserModel userImage)
        {
            bool isSuccess = false;
            int results = 0;

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    // Decode the User_Id
                    byte[] data = Convert.FromBase64String(userImage.User_Id);
                    string decodedString = System.Text.Encoding.UTF8.GetString(data);

                    using (SqlCommand cmd = new SqlCommand("SaveUserImage", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@User_Id", decodedString.Trim());
                        //cmd.Parameters.AddWithValue("@PImageNo", userImage.PImageNumber);
                        //cmd.Parameters.AddWithValue("@BImageNo", userImage.BImageNumber);
                        cmd.Parameters.AddWithValue("@PImageName", userImage.PImageName);
                        cmd.Parameters.AddWithValue("@BImageName", userImage.BImageName);
                        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;

                        await cmd.ExecuteNonQueryAsync();

                        results = Convert.ToInt32(cmd.Parameters["@Result"].Value);

                        isSuccess = results > 0 ? true : false;


                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }

            }
            return isSuccess;


        }

        public async Task<bool> UpdateUserImages(UserModel userImage)
        {
            bool isSuccess = false;

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    // Decode the User_Id
                    byte[] data = Convert.FromBase64String(userImage.User_Id);
                    string decodedString = System.Text.Encoding.UTF8.GetString(data);


                    // Retrieve existing file names 
                    string existingProfilePhotoName = await GetProfilePhotoName(decodedString);
                    string existingBannerPhotoName = await GetBannerPhotoName(decodedString);

                    // Delete existing files if new files are uploaded
                    if (userImage.PImage != null && !string.IsNullOrEmpty(existingProfilePhotoName))
                    {
                        string existingProfilePhotoPath = Path.Combine(_config["AppSettings:APIImageFolderP"], existingProfilePhotoName + ".jpeg");
                        if (System.IO.File.Exists(existingProfilePhotoPath))
                        {
                            System.IO.File.Delete(existingProfilePhotoPath);
                        }
                    }

                    if (userImage.BImage != null && !string.IsNullOrEmpty(existingBannerPhotoName))
                    {
                        string existingBannerPhotoPath = Path.Combine(_config["AppSettings:APIImageFolderB"], existingBannerPhotoName + ".jpeg");
                        if (System.IO.File.Exists(existingBannerPhotoPath))
                        {
                            System.IO.File.Delete(existingBannerPhotoPath);
                        }
                    }

                    // Update the database with the new  information
                    using (SqlCommand cmd = new SqlCommand("UpdateUserImages", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@User_Id", decodedString.Trim());
                        cmd.Parameters.AddWithValue("@PImageName", userImage.PImageName);
                        cmd.Parameters.AddWithValue("@BImageName", userImage.BImageName);
                        cmd.Parameters.Add("@Result", SqlDbType.Int).Direction = ParameterDirection.Output;

                        await cmd.ExecuteNonQueryAsync();

                        int results = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                        isSuccess = results > 0;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return isSuccess;
        }

        public async Task<bool> DeleteProfileImage(string user_Id)
        {
            bool isSuccess = false;

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    // Decode the User_Id
                    byte[] data = Convert.FromBase64String(user_Id);
                    string decodedString = System.Text.Encoding.UTF8.GetString(data);

                    // Retrieve existing profile photo name
                    string existingProfilePhotoName = await GetProfilePhotoName(decodedString);

                    if (!string.IsNullOrEmpty(existingProfilePhotoName))
                    {
                        // Delete existing profile photo
                        string existingProfilePhotoPath = Path.Combine(_config["AppSettings:APIImageFolderP"], existingProfilePhotoName + ".jpeg");
                        if (System.IO.File.Exists(existingProfilePhotoPath))
                        {
                            System.IO.File.Delete(existingProfilePhotoPath);
                        }

                        // Update the database to set the profile photo name to null
                        using (SqlCommand cmd = new SqlCommand("UpdateProfileImageName", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@User_Id", decodedString.Trim());
                            await cmd.ExecuteNonQueryAsync();
                        }

                        isSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return isSuccess;
        }

        public async Task<bool> DeleteBannerImage(string user_Id)
        {
            bool isSuccess = false;

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    // Decode the User_Id
                    byte[] data = Convert.FromBase64String(user_Id);
                    string decodedString = System.Text.Encoding.UTF8.GetString(data);

                    // Retrieve existing banner photo name
                    string existingBannerPhotoName = await GetBannerPhotoName(decodedString);

                    if (!string.IsNullOrEmpty(existingBannerPhotoName))
                    {
                        // Delete existing banner photo
                        string existingBannerPhotoPath = Path.Combine(_config["AppSettings:APIImageFolderB"], existingBannerPhotoName + ".jpeg");
                        if (System.IO.File.Exists(existingBannerPhotoPath))
                        {
                            System.IO.File.Delete(existingBannerPhotoPath);
                        }

                        // Update the database to set the banner photo name to null
                        using (SqlCommand cmd = new SqlCommand("UpdateBannerImageName", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@User_Id", decodedString.Trim());
                            await cmd.ExecuteNonQueryAsync();
                        }

                        isSuccess = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return isSuccess;
        }

        public async Task<UserModel> GetUserDetailsByUserId(string UserId)
        {
            UserModel user = null;

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    // Decode the User_Id
                    byte[] data = Convert.FromBase64String(UserId);
                    string decodedString = System.Text.Encoding.UTF8.GetString(data);


                    using (SqlCommand cmd = new SqlCommand("GetUserImageDetails", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@User_Id", decodedString.Trim());

                        using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (reader.Read())
                            {
                                // the result set
                                user = new UserModel
                                {
                                    User_Id = reader["User_Id"].ToString().Trim(),
                                    PImageName = reader["PImageName"] != DBNull.Value ? (Guid)reader["PImageName"] : Guid.Empty,
                                    BImageName = reader["BImageName"] != DBNull.Value ? (Guid)reader["BImageName"] : Guid.Empty,

                                };
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return user;
        }

        public async Task<string> GetProfilePhotoName(string user_Id)
        {
            string profilePhotoName = string.Empty;

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    using (SqlCommand cmd = new SqlCommand("SELECT PImageName FROM tblUserImages WHERE User_Id = @User_Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@User_Id", user_Id.Trim());

                        using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (reader.Read())
                            {
                                profilePhotoName = reader["PImageName"].ToString();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return profilePhotoName;
        }

        public async Task<string> GetBannerPhotoName(string user_Id)
        {
            string bannerPhotoName = string.Empty;

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    using (SqlCommand cmd = new SqlCommand("SELECT BImageName FROM tblUserImages WHERE User_Id = @User_Id", connection))
                    {
                        cmd.Parameters.AddWithValue("@User_Id", user_Id.Trim());

                        using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            if (reader.Read())
                            {
                                bannerPhotoName = reader["BImageName"].ToString();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

            return bannerPhotoName;
        }



    }
}
