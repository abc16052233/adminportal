﻿using Peercore.CRM.Planthire.API.Models;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public interface IRentalOrderDAL
    {
        Task<TransactionWrapper> GetRentalOrderInforToCustomerPortal(string toDate, string fromDate, string custCode, string catalogType = "");
    }
}
