﻿using Peercore.CRM.AdminPortal.API.CommonUtility;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public class OperatorProfileDAL : IOperatorProfileDAL
    {
        private readonly string _connectionStr;
        private readonly string profileBaseImageUrl;

        public OperatorProfileDAL(IConfiguration configuration)
        {
            _connectionStr = configuration.GetConnectionString("HTLite_DB_ConnectionStr");
            profileBaseImageUrl = configuration.GetSection("AppSettings")["OperatorProfileBaseImageUrl"];
        }

        public async Task<TransactionWrapper> GetOperatorProfile(int operatorId)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<OperatorProfileModel> operatorProfileData = new List<OperatorProfileModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetOperatorProfileInfo", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@OperatorId", operatorId);

                        await HelperUtility.GetRecords(ReadOperatorProfileDataRow, operatorProfileData, cmd);

                        if (operatorProfileData.Any())
                        {
                            //IEnumerable<OperatorProfileModel> _operatorProfile = ReadOperatorProfileDataGrouping(operatorProfileData);

                            List<OperatorSkillModel> updatedOperatorProfilewithSkills = new List<OperatorSkillModel>();
                            //foreach (var item in _operatorProfile)
                            //{
                            //    var updatedItem = await GetOperatorsSkills(item.EmployeeNumber);
                            //    updatedOperatorProfilewithSkills = (List<OperatorSkillModel>)updatedItem.Result;
                            //    item.OperatorsSkills = updatedOperatorProfilewithSkills;
                            //}

                            var updatedItem = await GetOperatorsSkills(operatorProfileData[0].EmployeeNumber);
                            updatedOperatorProfilewithSkills = (List<OperatorSkillModel>)updatedItem.Result;
                            operatorProfileData[0].OperatorsSkills = updatedOperatorProfilewithSkills;

                            wrapper.IsSuccess = true;
                            wrapper.Result = operatorProfileData[0];


                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        private OperatorProfileModel ReadOperatorProfileDataRow(IDataRecord record)
        {
            string profileImageName = HelperUtility.ReadStringValue(record, "image_file").Trim();

            return new OperatorProfileModel()
            {

                EmployeeNumber = HelperUtility.ReadNumberValue(record, "employee_no"),
                FirstName = HelperUtility.ReadStringValue(record, "first_name").Trim(),
                LastName = HelperUtility.ReadStringValue(record, "last_name").Trim(),
                DateOfBirth = HelperUtility.ReadDateValue(record, "date_of_birth").ToString("dd/MM/yyyy"),
                ContactNumber = HelperUtility.ReadStringValue(record, "contact_no").Trim(),
                ContactEmail = HelperUtility.ReadStringValue(record, "employee_email").Trim(),
                ProfileImage = ConvertImageUrlToBase64(profileBaseImageUrl, profileImageName),
                GDriveLink = HelperUtility.ReadStringValue(record, "GDriveLink").Trim()

            };
        }

        

        //Getting image Url
        private string ConvertImageUrlToBase64(string profileBaseImageUrl, string profileImageName)
        {
            string profileImageUrl = ConstructImageUrl(profileBaseImageUrl, profileImageName);

            if (profileImageUrl != "")
            {
                try
                {

                    using (WebClient webClient = new WebClient())
                    {
                        byte[] imageBytes = webClient.DownloadData(profileImageUrl);
                        return Convert.ToBase64String(imageBytes);
                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Error converting image URL to base64: {ex.Message}");
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        private string ConstructImageUrl(string profileBaseImageUrl, string profileImageName)
        {
            return $"{profileBaseImageUrl}/{profileImageName}";
        }

        //private IEnumerable<OperatorProfileModel> ReadOperatorProfileDataGrouping(IEnumerable<OperatorProfileModel> operatorProfileData)
        //{
        //    IEnumerable<OperatorProfileModel> data = new List<OperatorProfileModel>();
        //    data = operatorProfileData;

        //    return data;
        //}

        //getting skills of operators
        public async Task<TransactionWrapper> GetOperatorsSkills(int operatorId)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<OperatorSkillModel> operatorSkillData= new List<OperatorSkillModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetOperatorsSkills", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@OperatorId", operatorId);

                        await HelperUtility.GetRecords(ReadOperatorsSkillsDataRow, operatorSkillData, cmd);

                        if (operatorSkillData.Any())
                        {
                            IEnumerable<OperatorSkillModel> _operatorsSkills = ReadOperatorsSkillsDataGrouping(operatorSkillData);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _operatorsSkills;


                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        private OperatorSkillModel ReadOperatorsSkillsDataRow(IDataRecord record)
        {
            return new OperatorSkillModel()
            {
                SkillsDescription = HelperUtility.ReadStringValue(record, "skill_description").Trim()
            };
        }

        private IEnumerable<OperatorSkillModel> ReadOperatorsSkillsDataGrouping(IEnumerable<OperatorSkillModel> operatorSkillData)
        {
            IEnumerable<OperatorSkillModel> data = new List<OperatorSkillModel>();
            data = operatorSkillData;

            return data;
        }


    }
}
