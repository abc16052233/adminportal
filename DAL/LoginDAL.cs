﻿using Peercore.CRM.AdminPortal.API.CommonUtility;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System.Data;
using System.Data.SqlClient;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public class LoginDAL : ILoginDAL
    {
        private readonly string _connectionStr;

        public LoginDAL(IConfiguration configuration)
        {
            _connectionStr = configuration.GetConnectionString("HTLite_DB_ConnectionStr");
        }

        public async Task<TransactionWrapper> Login(string userName, string password)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<LoginModel> login = new List<LoginModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetCustomerPortalLoginByUNameAndPwd", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserName", userName);
                        cmd.Parameters.AddWithValue("@Password", password);

                        await HelperUtility.GetRecords(ReadLoginByUserNameAndPasswordDataRow, login, cmd);

                        if (login.Any())
                        {
                            IEnumerable<LoginModel> _asset = LoginDataGrouping(login);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _asset;
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        private static LoginModel ReadLoginByUserNameAndPasswordDataRow(IDataRecord record)
        {
            return new LoginModel()
            {
                CustCode = HelperUtility.ReadStringValue(record, "cust_code").Trim(),
                CustName = HelperUtility.ReadStringValue(record, "name").Trim(),
                Address1 = HelperUtility.ReadStringValue(record, "address_1").Trim(),
                Address2 = HelperUtility.ReadStringValue(record, "address_2").Trim(),
                City = HelperUtility.ReadStringValue(record, "city").Trim(),
                State = HelperUtility.ReadStringValue(record, "state").Trim(),
                PostCode = HelperUtility.ReadStringValue(record, "postcode").Trim(),
                Country = HelperUtility.ReadStringValue(record, "country").Trim(),
                Acn = HelperUtility.ReadStringValue(record, "acn").Trim(),
                Abn = HelperUtility.ReadStringValue(record, "abn").Trim(),
                Telephone = HelperUtility.ReadStringValue(record, "telephone").Trim(),
                Fax = HelperUtility.ReadStringValue(record, "fax").Trim(),
                Mobile = HelperUtility.ReadStringValue(record, "mobile").Trim(),
                IternetAdd = HelperUtility.ReadStringValue(record, "internet_add").Trim(),
                Contact = HelperUtility.ReadStringValue(record, "contact").Trim()
            };
        }

        private static IEnumerable<LoginModel> LoginDataGrouping(IEnumerable<LoginModel> loginData)
        {
            IEnumerable<LoginModel> data = new List<LoginModel>();
            data = loginData;

            return data;
        }

        public async Task<TransactionWrapper> InsertCustPortalSessionOntimeAsync(CustPortalSession session)
        {
            TransactionWrapper wrapper = new TransactionWrapper();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("InsertCustPortalSession", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@clientId", session.ClientId);
                        cmd.Parameters.AddWithValue("@locationLat", session.LocationLat);
                        cmd.Parameters.AddWithValue("@locationLon", session.LocationLng);
                        cmd.Parameters.AddWithValue("@device", session.Device);

                        HelperUtility.SetOutParameterResult(cmd);

                        await cmd.ExecuteNonQueryAsync();

                        HelperUtility.GetOutParameterResult(cmd, out ResultModel resultModel);
                        if (resultModel.Result == -1)
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("InsertCustPortalSessionAsync: Error when create Role");
                        }
                        else if (resultModel.Result == -2)
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("InsertCustPortalSessionAsync: Role name already exist");
                        }
                        else
                        {
                            session.SessionId = resultModel.Result;

                            wrapper.IsSuccess = true;
                            wrapper.ResultSet.Add(session);
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("InsertCustPortalSessionAsync: " + ex.Message);
                }
            }

            return wrapper;
        }

        public async Task<TransactionWrapper> UpdateCustPortalSessioOfftimenAsync(CustPortalSession session)
        {
            TransactionWrapper wrapper = new TransactionWrapper();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("UpdateCustPortalSession", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@sessionId", session.SessionId);
                        cmd.Parameters.AddWithValue("@locationLat", session.LocationLat);
                        cmd.Parameters.AddWithValue("@locationLng", session.LocationLng);
                        cmd.Parameters.AddWithValue("@device", session.Device);


                        HelperUtility.SetOutParameterResult(cmd);

                        await cmd.ExecuteNonQueryAsync();

                        HelperUtility.GetOutParameterResult(cmd, out ResultModel resultModel);
                        if (resultModel.Result == -1)
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("UpdateCustPortalSessionAsync: Error when create Role");
                        }
                        else if (resultModel.Result == -2)
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("UpdateCustPortalSessionAsync: Role name already exist");
                        }
                        else
                        {
                            wrapper.IsSuccess = true;
                            wrapper.ResultSet.Add(session);
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("UpdateCustPortalSessionAsync: " + ex.Message);
                }
            }

            return wrapper;
        }

        public async Task<TransactionWrapper> InsertCustPortalEventsAsync(CustPortalEvents events)
        {
            TransactionWrapper wrapper = new TransactionWrapper();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("InsertCustPortalEvent", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@sessionId", events.SessionId);
                        cmd.Parameters.AddWithValue("@actionType", events.ActionType);
                        cmd.Parameters.AddWithValue("@module", events.Module);
                        cmd.Parameters.AddWithValue("@description", events.Description);

                        HelperUtility.SetOutParameterResult(cmd);

                        await cmd.ExecuteNonQueryAsync();

                        HelperUtility.GetOutParameterResult(cmd, out ResultModel resultModel);
                        if (resultModel.Result == -1)
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("InsertCustPortalEventsAsync: Error when create Event");
                        }
                        else
                        {
                            events.EventId = resultModel.Result;

                            wrapper.IsSuccess = true;
                            wrapper.ResultSet.Add(events);
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("InsertCustPortalEventsAsync: " + ex.Message);
                }
            }

            return wrapper;
        }

        //added by shalinka

        public async Task<TransactionWrapper> GetSessionsBetweenDates(string startDate, string endDate)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<SessionModel> sessions = new List<SessionModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    using (SqlCommand cmd = new SqlCommand("GetAllSessions", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@StartDate", startDate);
                        cmd.Parameters.AddWithValue("@EndDate", endDate);


                        await HelperUtility.GetRecords(ReadSessionDataRow, sessions, cmd);

                        if (sessions.Any())
                        {
                            wrapper.IsSuccess = true;
                            wrapper.Result = sessions;
                        }
                        else
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("No sessions found.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetAllSessions: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        public async Task<TransactionWrapper> GetEventsBySessionId(int sessionId)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<EventModel> events = new List<EventModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    using (SqlCommand cmd = new SqlCommand("GetEventsBySessionId", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@SessionId", sessionId);

                        await HelperUtility.GetRecords(ReadEventDataRow, events, cmd);

                        if (events.Any())
                        {
                            wrapper.IsSuccess = true;
                            wrapper.Result = events;
                        }
                        else
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("No sessions found.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetEventsBySessionId: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        private static EventModel ReadEventDataRow(IDataRecord record)
        {
            return new EventModel()
            {
                EventId = HelperUtility.ReadNumberValue(record, "eventId"),
                SessionId = HelperUtility.ReadNumberValue(record, "sessionId"),
                ActionType = HelperUtility.ReadStringValue(record, "actionType").Trim(),
                Module = HelperUtility.ReadStringValue(record, "module").Trim(),
                Description = HelperUtility.ReadStringValue(record, "description").Trim(),
                EventDate = HelperUtility.ReadDateValue(record, "eventDate").ToString("dd/MM/yyyy hh:mm:ss tt"),
            };
        }

        private static SessionModel ReadSessionDataRow(IDataRecord record)
        {
            return new SessionModel()
            {
                SessionId = HelperUtility.ReadNumberValue(record, "sessionId"),
                ClientId = HelperUtility.ReadStringValue(record, "clientId").Trim(),
                ClientName = HelperUtility.ReadStringValue(record, "clientName").Trim(),
                SessionOnTime = HelperUtility.ReadDateValue(record, "sessionOnTiime").ToString("dd/MM/yyyy hh:mm:ss tt"),
                SessionOffTime = HelperUtility.ReadDateValue(record, "sessionOffTime").ToString("dd/MM/yyyy hh:mm:ss tt"),
                LocationLat = HelperUtility.ReadStringValue(record, "locationLat").Trim(),
                LocationLon = HelperUtility.ReadStringValue(record, "locationLon").Trim(),
                Device = HelperUtility.ReadStringValue(record, "device").Trim(),

            };
        }

        public async Task<TransactionWrapper> GetTotalEventsCountByUser()
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<UserEventsCount> events = new List<UserEventsCount>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    using (SqlCommand cmd = new SqlCommand("GetTotalEventsCountByUser", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (reader.Read())
                            {
                                UserEventsCount eventCount = ReadEventCountDataRow(reader);
                                events.Add(eventCount);
                            }
                        }

                        if (events.Any())
                        {
                            wrapper.IsSuccess = true;
                            wrapper.Result = events.GroupBy(e => e.UserId)
                                                   .Select(g => new
                                                   {
                                                       UserId = g.Key,
                                                       Events = g.Select(e => new UserEventsCount
                                                       {
                                                           // UserId = e.UserId,
                                                           Module = e.Module,
                                                           EventCount = e.EventCount
                                                       }).ToList()
                                                   }).ToList();
                        }
                        else
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("No events found.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetTotalEventsCountByUser: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        public async Task<TransactionWrapper> GetTotalEventsCountByModulesWithUsers()
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<UserEventsCount> modules = new List<UserEventsCount>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    await connection.OpenAsync();

                    using (SqlCommand cmd = new SqlCommand("GetTotalEventsCountByModulesWithUsers", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader reader = await cmd.ExecuteReaderAsync())
                        {
                            while (reader.Read())
                            {
                                UserEventsCount moduleWithUsersCount = ReadEventCountDataRow(reader);
                                modules.Add(moduleWithUsersCount);
                            }
                        }

                        if (modules.Any())
                        {
                            wrapper.IsSuccess = true;
                            wrapper.Result = modules.GroupBy(m => m.Module)
                                                   .Select(g => new
                                                   {
                                                       Module = g.Key,
                                                       Events = g.Select(e => new
                                                       {
                                                           UserId = e.UserId,
                                                           EventCount = e.EventCount
                                                       }).ToList()
                                                   }).ToList();
                        }
                        else
                        {
                            wrapper.IsSuccess = false;
                            wrapper.Messages.Add("No Modules found.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetTotalEventsCountByModulesWithUsers: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;
        }

        private static UserEventsCount ReadEventCountDataRow(IDataRecord record)
        {
            return new UserEventsCount()
            {
                UserId = HelperUtility.ReadStringValue(record, "UserId").Trim(),
                Module = HelperUtility.ReadStringValue(record, "module").Trim(),
                EventCount = HelperUtility.ReadNumberValue(record, "EventCount"),

            };
        }
    }
}