﻿using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public interface ILoginDAL
    {
        Task<TransactionWrapper> Login(string userName, string password);
        Task<TransactionWrapper> InsertCustPortalSessionOntimeAsync(CustPortalSession session);
        Task<TransactionWrapper> UpdateCustPortalSessioOfftimenAsync(CustPortalSession session);
        Task<TransactionWrapper> InsertCustPortalEventsAsync(CustPortalEvents events);

        //added by shalinka
        Task<TransactionWrapper> GetSessionsBetweenDates(string startDate, string endDate);
        Task<TransactionWrapper> GetEventsBySessionId(int sessionId);
        Task<TransactionWrapper> GetTotalEventsCountByUser();
        Task<TransactionWrapper> GetTotalEventsCountByModulesWithUsers();

    }
}