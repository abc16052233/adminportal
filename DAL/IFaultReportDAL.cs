﻿using Peercore.CRM.Planthire.API.Models;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public interface IFaultReportDAL
    {
        Task<TransactionWrapper> GetFaultReportByCustomerCode(string decodedCustCode, string? startDate, string? endDate, string? status);
    }
}
