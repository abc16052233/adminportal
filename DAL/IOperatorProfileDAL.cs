﻿using Peercore.CRM.Planthire.API.Models;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public interface IOperatorProfileDAL
    {
        Task<TransactionWrapper> GetOperatorProfile(int operatorId);
    }
}
