﻿using Peercore.CRM.AdminPortal.API.Models;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public interface IUserDAL
    {
        Task<bool> SaveUserImage(UserModel userImage);
        Task<bool> UpdateUserImages(UserModel userImage);
        Task<bool> DeleteProfileImage(string user_Id);
        Task<bool> DeleteBannerImage(string user_Id);
        Task<UserModel> GetUserDetailsByUserId(string UserId);
    }
}
