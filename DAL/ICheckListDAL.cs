﻿using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System.Threading.Tasks;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public interface ICheckListDAL
    {
        Task<TransactionWrapper> GetAllPCLHeaderByDateAndType(string custCode, string startDate = "", string endDate = "", string clientType = "", string responseType = "");
        Task<TransactionWrapper> GetAllPCLHeaderByCustomerCode(string custCode, string startDate = "", string endDate = "", string clientType = "", string responseType = "");
        Task<TransactionWrapper> GetAllPCLHeaderByPCLId(string custCode, string filterOption = "", string filterValue = "");
        Task<TransactionWrapper> GetAllPCLDetailsByPCLId(int PclId);
        Task<TransactionWrapper> GetChecklistOverview( string CustomerCode, List<string> datestringArray);
    }
}