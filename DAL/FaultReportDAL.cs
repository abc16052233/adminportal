﻿using Peercore.CRM.AdminPortal.API.CommonUtility;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System.Data.SqlClient;
using System.Data;
using static System.Net.Mime.MediaTypeNames;
using System.Net;
using System;
using System.IO;

namespace Peercore.CRM.AdminPortal.API.DAL
{
    public class FaultReportDAL : IFaultReportDAL
    {
        private readonly string _connectionStr;
        private readonly string baseUrl;

        public FaultReportDAL(IConfiguration configuration)
        {
            _connectionStr = configuration.GetConnectionString("HTLite_DB_ConnectionStr");
            baseUrl = configuration.GetSection("AppSettings")["BaseImageUrl"];
        }

        public async Task<TransactionWrapper> GetFaultReportByCustomerCode(string decodedCustCode, string? startDate , string? endDate, string? status)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            List<FaultReportModel> faultReportList = new List<FaultReportModel>();

            using (SqlConnection connection = new SqlConnection(_connectionStr))
            {
                try
                {
                    connection.Open();

                    using (SqlCommand cmd = new SqlCommand("GetFaultReportByCustomerCode", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CustomerCode", decodedCustCode);
                        cmd.Parameters.AddWithValue("@StartDate", startDate);
                        cmd.Parameters.AddWithValue("@EndDate", endDate);
                        cmd.Parameters.AddWithValue("@Status", status);


                        await HelperUtility.GetRecords(ReadFaultReportDataRow, faultReportList, cmd);



                        if (faultReportList.Any())
                        {
                            IEnumerable<FaultReportModel> _faultReportList = FaultReportDataGrouping(faultReportList);

                            wrapper.IsSuccess = true;
                            wrapper.Result = _faultReportList;


                        }
                    }
                }
                catch (Exception ex)
                {
                    wrapper.IsSuccess = false;
                    wrapper.Messages.Add("GetBussinessAlertCountAsync: " + ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }

            return wrapper;

        }

        private string ConstructImageUrl(string baseUrl, string imageName, string fileExtension)
        {
            // Complete image URL
            return $"{baseUrl}/{imageName}.{fileExtension}";
        }

        private string ConvertImageUrlToBase64(string baseUrl, string imageName, string fileExtension)
        {
            string imageUrl = ConstructImageUrl(baseUrl, imageName, fileExtension);

            if (imageName != "")
            {
                try
                {

                    using (WebClient webClient = new WebClient())
                    {
                        byte[] imageBytes = webClient.DownloadData(imageUrl);
                        return Convert.ToBase64String(imageBytes);
                    }

                }
                catch (Exception ex)
                {

                    Console.WriteLine($"Error converting image URL to base64: {ex.Message}");
                    return "";
                }
            }
            else
            {
                return "";
            }
        }


        private FaultReportModel ReadFaultReportDataRow(IDataRecord record)
        {
            string imageName1 = HelperUtility.ReadStringValue(record, "Image1").Trim();
            string fileExtension1 = "jpeg";

            string imageName2 = HelperUtility.ReadStringValue(record, "Image2").Trim();
            string fileExtension2 = "jpeg";

            return new FaultReportModel()
            {
                FrId = HelperUtility.ReadNumberValue(record, "FRId"),
                PlantNo = HelperUtility.ReadNumberValue(record, "PlantNo"),
                PlantCode = HelperUtility.ReadStringValue(record, "PlantCode").Trim(),
                PlantDescription = HelperUtility.ReadStringValue(record, "PlantDescription").Trim(),
                OrderId = HelperUtility.ReadNumberValue(record, "OrderId"),
                MeterReading = HelperUtility.ReadStringValue(record, "MeterReading").Trim(),
                Image1 = ConvertImageUrlToBase64(baseUrl, imageName1, fileExtension1),
                Image2 = ConvertImageUrlToBase64(baseUrl, imageName2, fileExtension1),
                SubmittedById = HelperUtility.ReadNumberValue(record, "SubmittedByID"),
                SubmittedBy = HelperUtility.ReadStringValue(record, "SubmittedBY").Trim(),
                ContactNo = HelperUtility.ReadStringValue(record, "ContactNo").Trim(),
                SubmittedDate = HelperUtility.ReadDateValue(record, "SubmittedDate").ToString("dd/MM/yyyy"),
                SubmittedType = HelperUtility.ReadStringValue(record, "SubmittedByType").Trim(),
                Comment = HelperUtility.ReadStringValue(record, "Comment").Trim(),
                Status = HelperUtility.ReadStringValue(record, "Status").Trim(),
                Severity = HelperUtility.ReadStringValue(record, "Severity").Trim(),
                Location = HelperUtility.ReadStringValue(record, "Location").Trim(),
                WorkOrderNo = HelperUtility.ReadStringValue(record, "WorkOrderNo").Trim(),
                //ResolvedBy = HelperUtility.ReadStringValue(record, "ResolvedBy").Trim(),
                ResolvedDate = HelperUtility.ReadDateValue(record, "ResolvedDate").ToString("dd/MM/yyyy"),
            };
        }

        private static IEnumerable<FaultReportModel> FaultReportDataGrouping(IEnumerable<FaultReportModel> faultReportData)
        {
            IEnumerable<FaultReportModel> data = new List<FaultReportModel>();
            data = faultReportData;

            return data;
        }

       
    }
}
