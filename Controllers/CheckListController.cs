﻿//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Peercore.CRM.AdminPortal.API.CommonUtility;
//using Peercore.CRM.AdminPortal.API.Controllers.Shared;
//using Peercore.CRM.AdminPortal.API.DAL;
//using Peercore.CRM.AdminPortal.API.Models;
//using Peercore.CRM.Planthire.API.Models;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Numerics;
//using System.Reflection;
//using System.Text;

//namespace Peercore.CRM.AdminPortal.API.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class CheckListController : BaseController
//    {
//        private readonly ICheckListDAL _checklist;

//        public CheckListController(ICheckListDAL checklist)
//        {
//            _checklist = checklist;
//        }

//        [HttpGet]
//        [Route("getallpclheaderbydateandtype")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetAllPCLHeaderByDateAndType(string custCode, string startDate = "", string endDate = "", string? clientType = "", string? responseType = "")
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            byte[] data = Convert.FromBase64String(custCode);
//            string decodedString = System.Text.Encoding.UTF8.GetString(data);

//            //string decodedString = custCode;

//            try
//            {
//                if (startDate == "" || endDate == "")
//                {
//                    endDate = DateTime.Now.ToString();
//                    startDate = DateTime.Parse(endDate).AddDays(-1).ToString();
//                }
//            }
//            catch
//            {
//                endDate = DateTime.Now.ToString();
//                startDate = DateTime.Parse(endDate).AddDays(-1).ToString();
//            }

//            wrapper = await _checklist.GetAllPCLHeaderByDateAndType(decodedString, startDate, endDate, clientType, responseType);

//            if (wrapper.Result == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No Records, Record"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }

//        [HttpGet]
//        [Route("getallpclheaderbycustomercode")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetAllPCLHeaderByCustomercode(string custCode, string startDate = "", string endDate = "", string? clientType = "", string? responseType = "")
//        {
//            List<TransactionWrapper> wrappers = new List<TransactionWrapper>();
//            TransactionWrapper returnWrapper = new TransactionWrapper();
//            TransactionWrapper checklistWrappers = new TransactionWrapper();
//            List<CheckListHeaderModel> allChecklists = new List<CheckListHeaderModel>();

//            if (custCode.Contains(","))
//            {
//                // multiple custCodes
//                string[] custCodes = custCode.Split(',');
//                foreach (string Code in custCodes)
//                {
//                    byte[] data = Convert.FromBase64String(Code);
//                    string decodedString = System.Text.Encoding.UTF8.GetString(data);

//                    try
//                    {
//                        if (startDate == "" || endDate == "")
//                        {
//                            endDate = DateTime.Now.ToString();
//                            startDate = DateTime.Parse(endDate).AddDays(-1).ToString();
//                        }
//                    }
//                    catch
//                    {
//                        endDate = DateTime.Now.ToString();
//                        startDate = DateTime.Parse(endDate).AddDays(-1).ToString();
//                    }


//                    checklistWrappers = await _checklist.GetAllPCLHeaderByCustomerCode(decodedString, startDate, endDate, clientType, responseType);

//                    if (checklistWrappers.IsSuccess == true)
//                    {
//                        List<CheckListHeaderModel> checklists = checklistWrappers.Result as List<CheckListHeaderModel>;
//                        allChecklists.AddRange(checklists);

//                        returnWrapper.IsSuccess = true;
//                        returnWrapper.Result = allChecklists;

//                    }

//                }

//            }
//            else
//            {
//                // All custCodes
//                string decodedString = "";
//                if (string.Equals(custCode, "all", StringComparison.OrdinalIgnoreCase))
//                {
//                    decodedString = custCode;
//                }
//                else
//                {
//                    // single custCode
//                    byte[] data = Convert.FromBase64String(custCode);
//                    decodedString = System.Text.Encoding.UTF8.GetString(data);
//                }


//                try
//                {
//                    if (startDate == "" || endDate == "")
//                    {
//                        endDate = DateTime.Now.ToString();
//                        startDate = DateTime.Parse(endDate).AddDays(-1).ToString();
//                    }
//                }
//                catch
//                {
//                    endDate = DateTime.Now.ToString();
//                    startDate = DateTime.Parse(endDate).AddDays(-1).ToString();
//                }


//                returnWrapper = await _checklist.GetAllPCLHeaderByCustomerCode(decodedString, startDate, endDate, clientType, responseType);


//                wrappers.Add(returnWrapper);
//            }


//            if (returnWrapper.ResultSet == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No Records, Record"));
//            }

//            if (returnWrapper.IsSuccess)
//            {
//                return new OkObjectResult(returnWrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrappers);
//            }

//            //if (wrappers.All(w => w.Result == null))
//            //{
//            //    return NotFound(HelperUtility.ObjectNotFound("No Records, Record"));
//            //}

//            //if (wrappers.All(w => w.IsSuccess))
//            //{
//            //    return new OkObjectResult(returnWrapper);
//            //}
//            //else
//            //{
//            //    return StatusCode(StatusCodes.Status500InternalServerError, wrappers);
//            //}
//        }

//        [HttpGet]
//        [Route("getallpclheaderbypclid")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetAllPCLHeaderByPCLId(string custCode, string filterOption = "", string filterValue = "")
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            byte[] data = Convert.FromBase64String(custCode);
//            string decodedString = System.Text.Encoding.UTF8.GetString(data);

//            wrapper = await _checklist.GetAllPCLHeaderByPCLId(decodedString, filterOption, filterValue);

//            if (wrapper.ResultSet == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No checklist found"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }

//        [HttpGet]
//        [Route("getallpcldetailsbypclid")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetAllPCLDetailsByPCLId(int PclId)
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            wrapper = await _checklist.GetAllPCLDetailsByPCLId(PclId);

//            if (wrapper.ResultSet == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No checklist details found"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }


//        [HttpGet]
//        [Route("getchecklistoverview")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetChecklistOverview([FromQuery] CheckListOverviewRequestModel request)
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            List<string> datestringArray = new List<string>();

//            for (DateTime currentDate = request.startdate; currentDate <= request.enddate; currentDate = currentDate.AddDays(1))
//            {
//                string dateString = currentDate.ToString("yyyy-MM-dd");
//                datestringArray.Add(dateString);
//            }

//            wrapper = await _checklist.GetChecklistOverview(request.CustomerCode, datestringArray);

//            if (wrapper.ResultSet == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No checklist overview found"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }


//        [HttpGet]
//        [Route("getcompletedchecklistanalytics")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetCompletedChecklistAnalytics([FromQuery] CheckListOverviewRequestModel request)
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            //request.startdate = DateTime.Now.AddMonths(-3);
//            //request.enddate = DateTime.Now;

//            List<string> datestringArray = new List<string>();

//            for (DateTime currentDate = request.startdate; currentDate <= request.enddate; currentDate = currentDate.AddDays(1))
//            {
//                string dateString = currentDate.ToString("yyyy-MM-dd");
//                datestringArray.Add(dateString);
//            }

//            wrapper = await _checklist.GetChecklistOverview(request.CustomerCode, datestringArray);

//            List<CheckListOverviewResponseModel> allchecklists = wrapper.ResultSet.OfType<CheckListOverviewResponseModel>().ToList();

//            // Group allchecklists by month
//            var groupedChecklists = allchecklists.GroupBy(checklist =>
//            {
//                var dateParts = checklist.Date.Split('-');
//                return new { Year = dateParts[0], Month = dateParts[1] };
//            });

//            int totalCompletedChecklistIDCount = 0;
//            int totalTotalPlants = 0;
//            Dictionary<string, double> monthlyPercentages = new Dictionary<string, double>();

//            foreach (var group in groupedChecklists)
//            {
//                foreach (var date in group)
//                {
//                    int completedChecklistIDCount = date.Plants.Count(plant => plant.ChecklistID != 0);
//                    int totalPlants = date.Plants.Count;

//                    // Add counts for the current date to the total counts
//                    totalCompletedChecklistIDCount += completedChecklistIDCount;
//                    totalTotalPlants += totalPlants;
//                }

//                string monthYear = $"{group.Key.Year}-{group.Key.Month}";
//                if (totalTotalPlants > 0)
//                {
//                    double completedChecklistPercentage = (double)totalCompletedChecklistIDCount / totalTotalPlants * 100;

//                    // Update the dictionary with the calculated percentage for the corresponding month
//                    monthlyPercentages.Add(monthYear, completedChecklistPercentage);
//                }
//                else
//                {
//                    double completedChecklistPercentage = 0.00;
//                    monthlyPercentages.Add(monthYear, completedChecklistPercentage);
//                }
//                totalCompletedChecklistIDCount = 0; totalTotalPlants = 0;
//            }

//            List<object> checkListAnalytics = new List<object>();

//            foreach (var entry in monthlyPercentages)
//            {
//                string monthYear = entry.Key;
//                DateTime parsedDate = DateTime.ParseExact(monthYear, "yyyy-M", CultureInfo.InvariantCulture);
//                string month = parsedDate.ToString("MMMM", CultureInfo.InvariantCulture);
//                string formattedPercentage = $"{entry.Value:F2}%";

//                var analyticsData = new
//                {
//                    x = month,
//                    y = formattedPercentage
//                };

//                checkListAnalytics.Add(analyticsData);
//            }

//            var result = new { checkListAnalytics };
//            List<object> resultList = result.checkListAnalytics.ToList();
//            wrapper.ResultSet = resultList;

//            if (wrapper.ResultSet == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No checklist overview found"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }
//    }
//}
