﻿//using Microsoft.AspNetCore.Mvc;
//using Peercore.CRM.AdminPortal.API.CommonUtility;
//using Peercore.CRM.AdminPortal.API.Controllers.Shared;
//using Peercore.CRM.AdminPortal.API.DAL;
//using Peercore.CRM.Planthire.API.Models;

//namespace Peercore.CRM.AdminPortal.API.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class FaultReportController : BaseController
//    {
//        private readonly IFaultReportDAL _faultreport;

//        public FaultReportController(IFaultReportDAL faultreport)
//        {
//            _faultreport = faultreport;
//        }

//        [HttpGet]
//        [Route("getfaultreportbycustomercode")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetFaultReportByCustomerCode(string custCode, string? startDate = null, string? endDate = null, string? status = " ")
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            byte[] data = Convert.FromBase64String(custCode);
//            string decodedCustCode = System.Text.Encoding.UTF8.GetString(data);


//            wrapper = await _faultreport.GetFaultReportByCustomerCode(decodedCustCode, startDate, endDate, status);

//            if (wrapper.Result == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No Records, Record"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }
//    }
//}
