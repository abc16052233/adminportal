﻿using Microsoft.AspNetCore.Mvc;
using Peercore.CRM.AdminPortal.API.CommonUtility;
using Peercore.CRM.AdminPortal.API.Controllers.Shared;
using Peercore.CRM.AdminPortal.API.DAL;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;

namespace Peercore.CRM.AdminPortal.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : BaseController
    {
        private readonly ILoginDAL _login;

        public LoginController(ILoginDAL login)
        {
            _login = login;
        }

        //[HttpGet]
        //[Route("login")]
        //[ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        //[ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        //[ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        //public async Task<IActionResult> Login(string userName, string password)
        //{
        //    TransactionWrapper wrapper = new TransactionWrapper();

        //    byte[] uname = Convert.FromBase64String(userName);
        //    string decodedUserName = System.Text.Encoding.UTF8.GetString(uname);

        //    byte[] pwd = Convert.FromBase64String(password);
        //    string decodedPassword = System.Text.Encoding.UTF8.GetString(pwd);

        //    wrapper = await _login.Login(decodedUserName, decodedPassword);


        //    if (wrapper.ResultSet == null)
        //    {
        //        return NotFound(HelperUtility.ObjectNotFound("Login Error"));
        //    }

        //    if (wrapper.IsSuccess)
        //    {
        //        return new OkObjectResult(wrapper);
        //    }
        //    else
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
        //    }
        //}

        [HttpGet]
        [Route("login")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Login(string userName, string password)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            TransactionWrapper sessionWrapper = new TransactionWrapper();


            byte[] uname = Convert.FromBase64String(userName);
            string decodedUserName = System.Text.Encoding.UTF8.GetString(uname);

            byte[] pwd = Convert.FromBase64String(password);
            string decodedPassword = System.Text.Encoding.UTF8.GetString(pwd);

            wrapper = await _login.Login(decodedUserName, decodedPassword);

            //Session creation 
            if (wrapper.ResultSet == null)
            {
                return NotFound(HelperUtility.ObjectNotFound("Login Error"));
            }

            if (wrapper.IsSuccess)
            {
               List<LoginModel> user = wrapper.Result as List<LoginModel>;

                CustPortalSession custPortalSession = new CustPortalSession
                {
                    ClientId = decodedUserName,
                };

                // Validate the request
                if (custPortalSession == null || string.IsNullOrWhiteSpace(custPortalSession.ClientId))
                {
                   return BadRequest(HelperUtility.BadRequest("Invalid request."));
                }

                // Check if the user already exists in the local database
                sessionWrapper = await _login.InsertCustPortalSessionOntimeAsync(custPortalSession);

                if (sessionWrapper.IsSuccess)
                {
                  CustPortalSession createdSession = sessionWrapper.ResultSet[0] as CustPortalSession;

                  user[0].SessionId = createdSession.SessionId;
                  return new OkObjectResult(wrapper);
                }
                else
                {
                     // If session creation fails, return an error response
                     return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError("Session creation failed."));
                }

            }
            else
            {
                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
            }
        }

        [HttpPost]
        [Route("create-session")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> InsertCustPortalSessionOntime([FromBody] CustPortalSession session)
        {
            TransactionWrapper wrapper = new TransactionWrapper();

            try
            {
                // Validate the request
                if (session == null || string.IsNullOrWhiteSpace(session.ClientId))
                {
                    return BadRequest(HelperUtility.BadRequest("Invalid request."));
                }

                // Check if the user already exists in the local database
                wrapper = await _login.InsertCustPortalSessionOntimeAsync(session);
                if (wrapper.IsSuccess == false)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError("Session creation failed."));
                }

                // Create successful
                return Created("created", HelperUtility.Created("Session created successfully."));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError(ex.Message));
            }
        }

        //[HttpPost]
        //[Route("update-session")]
        //[ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status201Created)]
        //[ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        //[ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status500InternalServerError)]
        //public async Task<IActionResult> UpdateCustPortalSessionOntime([FromBody] CustPortalSession session)
        //{
        //    TransactionWrapper wrapper = new TransactionWrapper();

        //    try
        //    {
        //        // Validate the request
        //        if (session == null || session.SessionId ==0)
        //        {
        //            return BadRequest(HelperUtility.BadRequest("Invalid request."));
        //        }

        //        // Check if the user already exists in the local database
        //        wrapper = await _login.UpdateCustPortalSessioOfftimenAsync(session);
        //        if (wrapper.IsSuccess == false)
        //        {
        //            return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError("Session update failed."));
        //        }

        //        // Create successful
        //        return Created("updated", HelperUtility.Created("Session update successfully."));
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError(ex.Message));
        //    }
        //}

        [HttpPost]
        [Route("update-session")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdateCustPortalSessionOntime([FromBody] CustPortalSession session)
        {
            TransactionWrapper wrapper = new TransactionWrapper();
            TransactionWrapper eventWrapper = new TransactionWrapper();


            try
            {
                // Validate the request
                if (session == null || session.SessionId == 0)
                {
                    return BadRequest(HelperUtility.BadRequest("Invalid request."));
                }

                // Check if the user already exists in the local database
                wrapper = await _login.UpdateCustPortalSessioOfftimenAsync(session);

                if (wrapper.IsSuccess == false)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError("Session update failed."));
                }

                if(wrapper.IsSuccess == true)
                {
                    foreach(var sessionItem in session.EventList)
                    {
                        sessionItem.SessionId = session.SessionId;

                        eventWrapper = await _login.InsertCustPortalEventsAsync(sessionItem);

                        if (eventWrapper.IsSuccess == false)
                        {
                            break;
                        }
                    }

                }

                // Create successful
                return Created("updated", HelperUtility.Created("Session updated and event inserted successfully."));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError(ex.Message));
            }
        }

        [HttpPost]
        [Route("create-event")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> InsertCustPortalEvent([FromBody] CustPortalEvents events)
        {
            TransactionWrapper wrapper = new TransactionWrapper();

            try
            {
                // Validate the request
                if (events == null || events.SessionId == 0)
                {
                    return BadRequest(HelperUtility.BadRequest("Invalid request."));
                }

                // Check if the user already exists in the local database
                wrapper = await _login.InsertCustPortalEventsAsync(events);
                if (wrapper.IsSuccess == false)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError("Event creation failed."));
                }

                // Create successful
                return Created("created", HelperUtility.Created("Event created successfully."));
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, HelperUtility.InternalServerError(ex.Message));
            }
        }

        //added by shalinka

        [HttpGet]
        [Route("geteventsbysessionId")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetEventsBySessionId(int sessionId)
        {
            var result = await _login.GetEventsBySessionId(sessionId);

            if (result.IsSuccess)
            {
                return Ok(result.Result);
            }
            else
            {
                return BadRequest(new { messages = result.Messages });
            }


        }

        [HttpGet]
        [Route("getsessionsbydaterange")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetSessionsByDateRange(string startDate, string endDate)
        {
            try
            {
                if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate))
                {
                    endDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    startDate = DateTime.Parse(endDate).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
            catch
            {
                endDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                startDate = DateTime.Parse(endDate).AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
            }

            var result = await _login.GetSessionsBetweenDates(startDate, endDate);

            if (result.IsSuccess)
            {
                return Ok(result.Result);
            }
            else
            {
                return BadRequest(new { messages = result.Messages });
            }
        }

        [HttpGet]
        [Route("gettotaleventscountbymodule")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetTotalEventsCountByModulesWithUsers()
        {
            try
            {
                var result = await _login.GetTotalEventsCountByModulesWithUsers();

                if (result.IsSuccess)
                {
                    return Ok(result.Result);
                }
                else
                {
                    return BadRequest(new { ErrorMessage = result.Messages.FirstOrDefault() });
                }
            }
            catch (Exception ex)
            {

                return StatusCode(500, new { ErrorMessage = "An error occurred while processing the request." });
            }
        }

        [HttpGet]
        [Route("gettotaleventscountbyuser")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetTotalEventsCountByUser()
        {
            var result = await _login.GetTotalEventsCountByUser();

            if (result.IsSuccess)
            {
                return Ok(result.Result);
            }
            else
            {
                return BadRequest(new { messages = result.Messages });
            }
        }


    }
}
