﻿//using Microsoft.AspNetCore.Mvc;
//using Peercore.CRM.AdminPortal.API.CommonUtility;
//using Peercore.CRM.AdminPortal.API.Controllers.Shared;
//using Peercore.CRM.AdminPortal.API.DAL;
//using Peercore.CRM.Planthire.API.Models;

//namespace Peercore.CRM.AdminPortal.API.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class RentalOrderController : BaseController
//    {
//        private readonly IRentalOrderDAL _rentalorder;

//        public RentalOrderController(IRentalOrderDAL rentalorder)
//        {
//            _rentalorder = rentalorder;
//        }

//        [HttpGet]
//        [Route("getrentalorders")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetRentalOrderInforToCustomerPortal(string toDate, string fromDate, string custCode, string? catalogType)
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            byte[] data = Convert.FromBase64String(custCode);
//            string decodedCustCodeString = System.Text.Encoding.UTF8.GetString(data);

//            wrapper = await _rentalorder.GetRentalOrderInforToCustomerPortal(toDate, fromDate, decodedCustCodeString, catalogType);


//            if (wrapper.Result == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No Records, Record"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }
//    }
//}
