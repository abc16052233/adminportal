﻿//using Microsoft.AspNetCore.Mvc;
//using Peercore.CRM.AdminPortal.API.CommonUtility;
//using Peercore.CRM.AdminPortal.API.Controllers.Shared;
//using Peercore.CRM.AdminPortal.API.DAL;
//using Peercore.CRM.Planthire.API.Models;

//namespace Peercore.CRM.AdminPortal.API.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class OperatorProfileController : BaseController
//    {
//        private readonly IOperatorProfileDAL _operatorprofile;

//        public OperatorProfileController(IOperatorProfileDAL operatorprofile)
//        {
//            _operatorprofile = operatorprofile;
//        }

//        [HttpGet]
//        [Route("getoperatorprofile")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetOperatorProfile(int operatorId)
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            wrapper = await _operatorprofile.GetOperatorProfile(operatorId);


//            if (wrapper.Result == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No Records, Record"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }
//    }
//}
