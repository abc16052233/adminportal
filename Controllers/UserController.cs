﻿using Microsoft.AspNetCore.Mvc;
using Peercore.CRM.AdminPortal.API.DAL;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;

namespace Peercore.CRM.AdminPortal.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {


        private readonly IUserDAL _user;
        private readonly ILogger<UserController> _logger;
        private readonly IConfiguration _configuration;

        public UserController(IUserDAL user, IConfiguration configuration, ILogger<UserController> logger)
        {
            _user = user;
            _logger = logger;
            _configuration = configuration;
        }


        [HttpPost]
        [Route("adduserimages")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> SaveUserImage()
        {
            bool isSuccess = false;
            TransactionWrapper wrapper = new TransactionWrapper();

            try
            {
                var uploadedFileP = HttpContext.Request.Form.Files["ProfilePhoto"];
                var uploadedFileB = HttpContext.Request.Form.Files["BannerPhoto"];
                string user_Id = HttpContext.Request.Form["user_Id"];
                //string imageNoP = HttpContext.Request.Form["imageNoP"];
                //string imageNoB = HttpContext.Request.Form["imageNoB"];

                if (string.IsNullOrEmpty(user_Id))
                {
                    return BadRequest("User Id is required");
                }

                UserModel userImage = new UserModel();
                userImage.User_Id = user_Id;
                // userImage.PImageNumber = string.IsNullOrEmpty(imageNoP.Trim()) ? 1 : Convert.ToInt32(imageNoP);
                //userImage.BImageNumber = string.IsNullOrEmpty(imageNoB.Trim()) ? 1 : Convert.ToInt32(imageNoB);
                userImage.PImageName = Guid.NewGuid();
                userImage.BImageName = Guid.NewGuid();

                if (HttpContext.Request.Form.Files != null && HttpContext.Request.Form.Files.Count > 0)
                {
                    //var uploadedFileP = HttpContext.Request.Form.Files["ProfilePhoto"];
                    //var uploadedFileB = HttpContext.Request.Form.Files["BannerPhoto"];

                    using (var memoryStream = new MemoryStream())
                    {
                        uploadedFileP.CopyTo(memoryStream);
                        userImage.PImage = memoryStream.ToArray();

                        // Rewind the memory stream to the beginning
                        //memoryStream.Seek(0, SeekOrigin.Begin);
                        memoryStream.Seek(0, SeekOrigin.Begin);

                        uploadedFileB.CopyTo(memoryStream);
                        userImage.BImage = memoryStream.ToArray();
                    }

                    string formattedGuidP = userImage.PImageName.ToString();
                    string formattedGuidB = userImage.BImageName.ToString();

                    string filePathP = Path.Combine(_configuration["AppSettings:APIImageFolderP"], formattedGuidP + ".jpeg");
                    System.IO.File.WriteAllBytes(filePathP, userImage.PImage);

                    string filePathB = Path.Combine(_configuration["AppSettings:APIImageFolderB"], formattedGuidB + ".jpeg");
                    System.IO.File.WriteAllBytes(filePathB, userImage.BImage);

                    isSuccess = await _user.SaveUserImage(userImage);

                    if (isSuccess)
                    {
                        return new OkObjectResult(userImage);
                    }
                    else
                    {

                        return StatusCode(StatusCodes.Status500InternalServerError, "Failed to save user image.");
                    }
                }
                else
                {
                    return BadRequest("No file uploaded.");
                }
            }
            catch (Exception e)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }


        [HttpPut]
        [Route("updateuserimages")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> UpdateUserImages()
        {
            try
            {
                var uploadedFileP = HttpContext.Request.Form.Files["ProfilePhoto"];
                var uploadedFileB = HttpContext.Request.Form.Files["BannerPhoto"];
                string user_Id = HttpContext.Request.Form["user_Id"];

                if (string.IsNullOrEmpty(user_Id))
                {
                    return BadRequest("User Id is required");
                }

                UserModel userImage = new UserModel();
                userImage.User_Id = user_Id;
                userImage.PImageName = Guid.NewGuid();
                userImage.BImageName = Guid.NewGuid();

                if (HttpContext.Request.Form.Files != null && HttpContext.Request.Form.Files.Count > 0)
                {
                    if (uploadedFileP != null)
                    {
                        using (var memoryStreamP = new MemoryStream())
                        {
                            uploadedFileP.CopyTo(memoryStreamP);
                            userImage.PImage = memoryStreamP.ToArray();

                            string formattedGuidP = userImage.PImageName.ToString();
                            string filePathP = Path.Combine(_configuration["AppSettings:APIImageFolderP"], formattedGuidP + ".jpeg");
                            System.IO.File.WriteAllBytes(filePathP, userImage.PImage);
                        }
                    }

                    if (uploadedFileB != null)
                    {
                        using (var memoryStreamB = new MemoryStream())
                        {
                            uploadedFileB.CopyTo(memoryStreamB);
                            userImage.BImage = memoryStreamB.ToArray();

                            string formattedGuidB = userImage.BImageName.ToString();
                            string filePathB = Path.Combine(_configuration["AppSettings:APIImageFolderB"], formattedGuidB + ".jpeg");
                            System.IO.File.WriteAllBytes(filePathB, userImage.BImage);
                        }
                    }

                    bool isSuccess = await _user.UpdateUserImages(userImage);

                    if (isSuccess)
                    {
                        return new OkObjectResult(userImage);
                    }
                    else
                    {
                        return StatusCode(StatusCodes.Status500InternalServerError, "Failed to update user images.");
                    }
                }
                else
                {
                    return BadRequest("No file uploaded.");
                }
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }


        [HttpDelete]
        [Route("deleteuserimages")]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteUserImages(string user_Id, bool deleteProfile, bool deleteBanner)
        {
            try
            {
                if (deleteProfile)
                {
                    await _user.DeleteProfileImage(user_Id);
                }

                if (deleteBanner)
                {
                    await _user.DeleteBannerImage(user_Id);
                }

                return Ok("Images deleted successfully.");
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }

        [HttpGet]
        [Route("getuser")]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUserDetailsByUserId(string UserId)
        {
            try
            {
                UserModel user = await _user.GetUserDetailsByUserId(UserId);

                if (user != null)
                {
                    return Ok(user);
                }
                else
                {
                    return NotFound("User not found.");
                }
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }


    }
}


 