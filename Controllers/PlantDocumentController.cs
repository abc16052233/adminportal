﻿//using Microsoft.AspNetCore.Mvc;
//using Peercore.CRM.AdminPortal.API.CommonUtility;
//using Peercore.CRM.AdminPortal.API.Controllers.Shared;
//using Peercore.CRM.AdminPortal.API.DAL;
//using Peercore.CRM.Planthire.API.Models;

//namespace Peercore.CRM.AdminPortal.API.Controllers
//{
//    [Route("api/[controller]")]
//    [ApiController]
//    public class PlantDocumentController : BaseController
//    {

//        private readonly IPlantDocumentDAL _plantdocument;

//        public PlantDocumentController(IPlantDocumentDAL plantdocument)
//        {
//            _plantdocument = plantdocument;
//        }

//        [HttpGet]
//        [Route("getplantdocuments")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetPlantDocumentUrl(int plantNo)
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            wrapper = await _plantdocument.GetPlantDocumentUrl(plantNo);


//            if (wrapper.Result == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No Records, Record"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }

//        [HttpGet]
//        [Route("getmachineinfo")]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status200OK)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status404NotFound)]
//        [ProducesResponseType(typeof(TransactionWrapper), StatusCodes.Status400BadRequest)]
//        public async Task<IActionResult> GetMachineInfo(int plantNo)
//        {
//            TransactionWrapper wrapper = new TransactionWrapper();

//            wrapper = await _plantdocument.GetMachineInfo(plantNo);


//            if (wrapper.Result == null)
//            {
//                return NotFound(HelperUtility.ObjectNotFound("No Records, Record"));
//            }

//            if (wrapper.IsSuccess)
//            {
//                return new OkObjectResult(wrapper);
//            }
//            else
//            {
//                return StatusCode(StatusCodes.Status500InternalServerError, wrapper);
//            }
//        }

//    }
//}
