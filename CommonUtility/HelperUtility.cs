﻿using Microsoft.AspNetCore.Mvc;
using Peercore.CRM.AdminPortal.API.Models;
using Peercore.CRM.Planthire.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Peercore.CRM.AdminPortal.API.CommonUtility
{
    public static class HelperUtility
    {
        public static readonly string ROW_COUNT_TEXT = "@rowCount";
        public static readonly string RESULT_TEXT = "@Result";
        public static readonly object NOT_FOUND_TEXT = $" is not found.";

        public delegate T ReadRow<T>(IDataRecord record);
       
        /// <summary>
        /// Method to set out parameter with rowcount  to the command 
        /// </summary>
        public static void SetOutParameterRowCount(SqlCommand cmd)
        {
            
            cmd.Parameters.Add(ROW_COUNT_TEXT, SqlDbType.Int).Direction = ParameterDirection.Output;
        }

        //<summary>
        //dynamic Method to execute data from command 
        /// </summary>
        public static async Task GetRecords<T>(ReadRow<T> readRow, List<T> list, SqlCommand command)
        {
            SqlDataReader reader = await command.ExecuteReaderAsync();
            if (reader != null)
            {
                _ = ReadDataFromReader(reader, readRow, list);
            }
        }
        /// <summary>
        //dynamic Method to read data from the reader 
        /// </summary>
        private static List<T> ReadDataFromReader<T>(SqlDataReader reader, ReadRow<T> readRow, List<T> list)
        {
            while (reader.Read())
            {
                var item = readRow(reader);
                if (item != null)
                {
                    list.Add(item);
                }
            }
            reader.Close();
            return list;
        }
        /// <summary>
        /// Method to get out parameter with rowcount  to the command 
        /// </summary>
        public static void GetOutParameterRowCount(SqlCommand cmd, out ResultModel resultModel)
        {
            resultModel = new ResultModel();
            resultModel.RowCount = ReadNumberValue(cmd, ROW_COUNT_TEXT);
        }
      
        /// <summary>
        //method to convert command data to string
        /// </summary>
        public static string ReadStringValue(SqlCommand cmd, string columnName) =>
                                             (Convert.IsDBNull(cmd.Parameters[columnName].Value)) ? null : Convert.ToString(cmd.Parameters[columnName].Value);
        //<summary>
        //method to convert command data to int
        /// </summary>
        public static int ReadNumberValue(SqlCommand cmd, string columnName) =>
                                            (Convert.IsDBNull(cmd.Parameters[columnName].Value)) ? 0 : Convert.ToInt32(cmd.Parameters[columnName].Value);
        //<summary>
        //method to convert command data to date
        /// </summary>
        public static DateTime ReadDateValue(SqlCommand cmd, string columnName) =>
                                            (Convert.IsDBNull(cmd.Parameters[columnName].Value)) ? DateTime.MinValue : Convert.ToDateTime(cmd.Parameters[columnName].Value);

        //<summary>
        //method to convert command data to float
        /// </summary>
        public static float ReadFloatValue(SqlCommand cmd, string columnName) =>
                                            (Convert.IsDBNull(cmd.Parameters[columnName].Value)) ? 0 : (float)Convert.ToDouble(cmd.Parameters[columnName].Value);


        //<summary>
        //method to convert idata record to int
        /// </summary>
        public static int ReadNumberValue(IDataRecord record, string columnName) =>
                        record.IsDBNull(record.GetOrdinal(columnName)) ? 0 : Convert.ToInt32(record[columnName]);
        //<summary>
        //method to convert idata record to string 
        /// </summary>
        public static string ReadStringValue(IDataRecord record, string columnName) =>
                        record.IsDBNull(record.GetOrdinal(columnName)) ? "" : Convert.ToString(record[columnName]);

        //<summary>
        //method to convert idata record to date
        /// </summary>
        public static DateTime ReadDateValue(IDataRecord record, string columnName) =>
                        record.IsDBNull(record.GetOrdinal(columnName)) ? DateTime.MinValue : Convert.ToDateTime(record[columnName]);

        //<summary>
        //method to convert idata record to date
        /// </summary>
        public static float ReadFloatValue(IDataRecord record, string columnName) =>
                        record.IsDBNull(record.GetOrdinal(columnName)) ? 0: (float)Convert.ToDouble(record[columnName]);

        //<summary>
        //method to convert idata record to date
        /// </summary>
        public static bool ReadBooleanValue(IDataRecord record, string columnName) =>
                        record.IsDBNull(record.GetOrdinal(columnName)) ? false : Convert.ToBoolean(record[columnName]);


        /// <summary>
        /// Method to set out  parameter with result id to the command 
        /// </summary>
        public static void SetOutParameterResult(SqlCommand cmd)
        {
            
            cmd.Parameters.Add(RESULT_TEXT, SqlDbType.Int).Direction = ParameterDirection.Output;
        }
        /// <summary>
        /// Method to get out  parameter with result id to the command 
        /// </summary>
        public static void GetOutParameterResult(SqlCommand cmd, out ResultModel resultModel)
        {
            resultModel = new ResultModel();
            resultModel.Result = ReadNumberValue(cmd, RESULT_TEXT);
        }

        /// <summary>
        /// Method to set out  parameter with result id to the command 
        /// </summary>
        public static string ObjectNotFound(string objName)
        {
            return objName + NOT_FOUND_TEXT.ToString();
        }

        internal static Task GetRecords(Func<IDataRecord, PlantDetailsOverview> readChecklistOverviewDataRow, PlantDetailsOverview plantDetails, SqlCommand cmd)
        {
            throw new NotImplementedException();
        }

        public static TransactionWrapper BadRequest(string errorMessage)
        {
            return new TransactionWrapper
            {
                IsSuccess = false,
                Messages = new List<string> { errorMessage },
                // You might want to include additional information in the response
            };
        }

        public static TransactionWrapper InternalServerError(string errorMessage)
        {
            return new TransactionWrapper
            {
                IsSuccess = false,
                Messages = new List<string> { errorMessage },
                // You might want to include additional information in the response
            };
        }

        public static TransactionWrapper Created(string errorMessage)
        {
            return new TransactionWrapper
            {
                IsSuccess = true,
                Messages = new List<string> { errorMessage },
                // You might want to include additional information in the response
            };
        }
    }
}
